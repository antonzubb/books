<?php

namespace App\Repositories;

use App\Models\User;

/**
 * Class UserRepository
 * @package App\Repositories
 */
class UserRepository extends AbstractRepository
{
    public const MODEL_REPOSITORY = User::class;
}
