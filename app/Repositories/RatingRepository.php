<?php

namespace App\Repositories;

use App\Models\Rating;

/**
 * Class UserRepository
 * @package App\Repositories
 */
class RatingRepository extends AbstractRepository
{
    public const MODEL_REPOSITORY = Rating::class;
}
