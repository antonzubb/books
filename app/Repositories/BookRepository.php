<?php

namespace App\Repositories;

use App\Models\Book;

/**
 * Class UserRepository
 * @package App\Repositories
 */
class BookRepository extends AbstractRepository
{
    public const MODEL_REPOSITORY = Book::class;
}
