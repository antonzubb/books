<?php


namespace App\DTO;


use Spatie\DataTransferObject\DataTransferObject;

class BookDto extends DataTransferObject
{
    /** @var string */
    public $title;

    /** @var string */
    public $description;

    /** @var int */
    public $user_id;
}
