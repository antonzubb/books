<?php


namespace App\DTO;


use Spatie\DataTransferObject\DataTransferObject;

class RatingDto extends DataTransferObject
{
    /** @var int */
    public $rating;

    /** @var int */
    public $book_id;
}
