<?php

namespace App\Policies;

use App\Models\Book;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use phpDocumentor\Reflection\Types\Mixed_;

class BookPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @param User $user
     * @param Book $book
     * @return bool
     */
    public function manipulate(User $user, Book $book)
    {
        return $user->id == $book->user_id;
    }
}
