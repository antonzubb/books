<?php


namespace App\Services;

use App\Models\User;
use App\Repositories\UserRepository;

class UserService
{

    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function create(array $credentials): User
    {
       return  $this->userRepository->create($this->prepareCredentials($credentials));
    }

    protected function prepareCredentials($credentials): array
    {
         $credentials['password'] = bcrypt($credentials['password']);
         return $credentials;
    }
}
