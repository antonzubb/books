<?php


namespace App\Services;


use App\DTO\BookDto;
use App\DTO\RatingDto;
use App\Models\Book;
use App\Models\Rating;
use App\Repositories\BookRepository;
use App\Repositories\RatingRepository;
use Illuminate\Pagination\LengthAwarePaginator;

class RatingsService
{

    /**
     * @var RatingRepository
     */
    private $ratingRepository;

    public function __construct(RatingRepository $ratingRepository)
    {
        $this->ratingRepository = $ratingRepository;
    }


    public function create(RatingDto $dto): Rating
    {
        return $this->ratingRepository->create($dto->toArray());
    }

}
