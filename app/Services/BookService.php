<?php


namespace App\Services;


use App\DTO\BookDto;
use App\Models\Book;
use App\Repositories\BookRepository;
use Illuminate\Pagination\LengthAwarePaginator;

class BookService
{
    /**
     * @var BookRepository
     */
    private $bookRepository;

    public function __construct(BookRepository $bookRepository)
    {
        $this->bookRepository = $bookRepository;
    }

    public function all(): ?LengthAwarePaginator
    {
        return $this->bookRepository->getAllWithRelations(['ratings']);
    }

    public function create(BookDto $dto): Book
    {
        return $this->bookRepository->create($dto->toArray());
    }

    public function update(BookDto $dto, Book $book): Book
    {
        return $this->bookRepository->updateByArray($book, $dto->toArray());
    }

    public function item(array $params): ?Book
    {
        return $this->bookRepository->find($params);
    }

    public function delete(Book $book): ?bool
    {
        return $this->bookRepository->delete($book);
    }
}
