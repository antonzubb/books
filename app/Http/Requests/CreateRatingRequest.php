<?php

namespace App\Http\Requests;

use App\DTO\BookDto;
use App\DTO\RatingDto;
use Illuminate\Foundation\Http\FormRequest;

class CreateRatingRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return  auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'rating' => 'required|min:1|int',
            'book_id' => 'required|min:1|int',
        ];
    }

    protected function toDto(): RatingDto
    {
        return new RatingDto(array_merge($this->validated(), ['user_id' => auth()->id()]));
    }
}
