<?php

namespace App\Http\Requests;

use App\DTO\BookDto;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Support\Facades\Gate;


class UpdateBookRequest extends ApiRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return Gate::allows('manipulate', $this->route('book'));
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
             'title' => 'min:3',
             'description' => 'min:12',
        ];
    }

    public function toDto(): BookDto
    {
        return new BookDto(array_merge($this->validated(), ['user_id' => auth()->id()]));
    }
}
