<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateRatingRequest;
use App\Models\Book;
use App\Http\Resources\RatingResource;
use App\Models\User;
use App\Services\RatingsService;


class RatingController extends Controller
{
    /**
     * @var RatingsService
     */
    private $ratingsService;

    public function __construct(RatingsService $ratingsService)
    {
        $this->ratingsService = $ratingsService;
    }

    public function store(CreateRatingRequest $request, Book $book): RatingResource
    {
        $rating = $this->ratingsService->create($request->toDto());

        return new RatingResource($rating);
    }
}
