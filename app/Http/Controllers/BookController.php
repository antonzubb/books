<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateBookRequest;
use App\Http\Requests\UpdateBookRequest;
use App\Models\Book;
use App\Http\Resources\BookResource;
use App\Services\BookService;

class BookController extends Controller
{
    /**
     * @var BookService
     */
    private $bookService;

    public function __construct(BookService $bookService)
    {
        $this->middleware('auth:api')->except(['index', 'show']);
        $this->bookService = $bookService;
    }

    public function index()
    {
        return BookResource::collection($this->bookService->all());
    }

    public function store(CreateBookRequest $request): BookResource
    {
        $book = $this->bookService->create($request->toDto());

        return new BookResource($book);
    }

    public function show(Book $book): BookResource
    {
        return new BookResource($book);
    }

    public function update(UpdateBookRequest $request, Book $book): BookResource
    {
        $this->bookService->update($request->toDto(), $book);

        return new BookResource($book);
    }

    public function destroy(Book $book)
    {
        $this->bookService->delete($book);

        return response()->json(null, 204);
    }
}
